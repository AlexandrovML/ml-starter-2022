const wWidth = window.innerWidth;
const wHeight = window.innerHeight;
const root = document.documentElement;

root.style.setProperty('--wWidth', `${wWidth}px`);
root.style.setProperty('--wHeight', `${wHeight}px`);