// Типы данных в JS
undefined;
null;

// Boolean
true
false

// string
'Hello world!'
// ""
// ``

//number
// 18;
// 3.14;

// Object
// объект
// массив
// функции
// ошибка


// Переменные в языке программирования
// const x = 1000 * 5 + 99 - 6;

// console.log(x);

// const y = x + 100;
// console.log(y)

// const firstName = 'John';
// const lastName = 'Doe';
// let age = 33;
// age = 34;
// console.log(age);

// const isAdult = true;


// математические операции
// -, +, *, /, %, **

// Унарные операторы
// -, +,
// -1
// -0
// +0
// 0
// + "2"    =   2


// Конкатенация +
// "hello" + "World"  // hello world
// 2 + "2"   // "22"

// Инкремент, декремент
// --, ++
// let x = 1;
// console.log(x++)   // 1

// x-- === (x = x-1)


// Логические выражения
// >, <, >=, <=, !=, !==, ==, ===

// console.log('is 5 > 2', 5>2)
//
// console.log('is equal: 5 and 5', 5 == 5);
// console.log('is equal: 5 and 4', 5 == 4);
//
// console.log('is equal: 5 and 5', 5 === '5');
//
// console.log('is not equal: 10 and 8', 10 != 8);
// console.log('is not equal: 10 and "10"', 10 !== "10");



// оператор switch
// let status = 'idle';
//
// switch (status) {
//     case 'idle':
//         console.log('status is sucess');
//         break;
//     case 'loading':
//         console.log('data is loading');
//         break;
//     case 'respond':
//         console.log('We have data');
//         break;
//     case 'error':
//         console.log('error');
//         break;
//     default:
//         console.log('default');
// }

// Тернарный оператор (выражение)
// const age = 15;
// age >= 18 ? console.log('sucess') : console.log('error')
//
// let isAdult = age >= 18 ? true : false;
// console.log(isAdult)
//
// let status = 'idle';
// let statusNumber = status === 'idle' ? 0 : 1;
// console.log({statusNumber});  // 0

// массивы Array
// const numbers = [12, 32, 38, 41, 55];
// console.log(numbers[0]);
// numbers[0] = 18;
// console.log(numbers[0]);
// console.log(numbers.length);
//
// const strs = ['abc', 'bcd'];
// // strs[2] = 'sd';
// strs.push('new item');
// console.log(strs);

// циклы Loop
// const numbers = [1, 2, 54, 88, 92, 100];
// let index = 0;
// while (index < numbers.length) {
//     numbers[index] *=2;
//     console.log(numbers[index]);
//     index++;
// }
// console.log('Hello after while')

// Цикл for
// for(let i = 0; i < numbers.length; i++) {
//     numbers[i] *= 2;
//     console.log(numbers[i]);
// }
// console.log('finish')

// оператор break
// let tries = 0;
// while(tries < 3) {
//      const age = +prompt('Сколько вам лет?');
//
//      if (age) {
//          alert('Welcome');
//          break;
//      }
//      tries++;
//
//      alert('Данные введены некоректно.');
// }
// alert('End');


// оператор continue
// for (let i = 0; i < 10; i++) {
//     if (i % 2 === 0) continue;
//
//     console.log(i);
// }

// function
// function sum(a, b) {
//     return a + b;
// }
// const x = sum(1, 5);
//
// function getNumberData(question) {
//     while(true) {
//         const res = +prompt(question);
//
//         if (res === 0 || res) {
//             return res;
//         }
//     }
// }
//
// const age = getNumberData('Сколько тебе лет?');
// console.log(age);

/*for(let i=0; i<10; i++) {
    console.log(i);
}*/























